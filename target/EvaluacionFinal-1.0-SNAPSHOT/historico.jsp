
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="root.model.dao.PalabrasJpaController"%>
<%@page import="root.persistence.entities.Palabras"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script src="jquery-2.0.3.min.js"></script>
        <script src="bootstrap.min.js"></script>
        <title>Buscador de Palabras</title>
    </head>
    <body>
    <%
            PalabrasJpaController dao = new PalabrasJpaController();  
            int cantidadPalabras = dao.getPalabrasCount(); 
    %>
    <DIV class="container">
            <h5>Palabras Buscadas con Exito: <%=cantidadPalabras%> </h5>
            <hr> 
            <table class="table table-bordered">
                <tr>
                    <th class="text-center">Palabra</th>
                    <th class="text-center">Descripci�n</th>
                </tr>
                
                
    <%
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        
        List<Palabras> getPalabras = dao.findPalabrasEntities(); 
        int indice = cantidadPalabras - 1;
            if (indice < 0) {
                indice = 0;
            }
        if (cantidadPalabras>0) {
                    for(int i=0;i<=indice;i++) {
                        out.write("<td class='text-center'>" + getPalabras.get(i).getPalabra()+ "</td>");
                        out.write("<td class='text-left'><ol>" + getPalabras.get(i).getSignificado()+ "</ol></td>");
                        out.write("</tr>");
                    }    
                } else {
                     out.write("<tr>");
                     out.write("<td class='text-center'> NO SE HAN BUSCADO PALABRAS A�N </td> ");
                     out.write("</tr>");
                }    
                
    %>
           
                
            
            </table>
             <table class="text-center">
                <td class="text-center"><a class="btn btn-primary" align="center" href="index.jsp" target="_top">Volver</a></td>
            </table>
    </DIV>
            
            
    </body>
</html>
