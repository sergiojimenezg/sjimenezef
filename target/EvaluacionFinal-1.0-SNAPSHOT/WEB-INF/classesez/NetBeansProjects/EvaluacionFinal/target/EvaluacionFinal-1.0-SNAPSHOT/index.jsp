

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <div class="container">
        <form action="controller" method="POST">
            <table class="table table-hover">
                <tr>
                    <td class="text-center"><h5>Examen Final - Taller de Aplicaciones</h5></td>
                </tr>
                <tr>
                    <td class="text-center"><h5>Alumno: Sergio Jimenez G.</h5></td>
                </tr>
                <tr>
                    <td class="text-center"><h6>Ingrese Palabra a buscar</h6></td>
                </tr>
                <tr>
                    <td class="text-center"><input type="text" name="palabra" value="" size="20" maxlength="20" required/></td>
                </tr>
                <tr>
                    <td class="text-center">
                        <input type="text" hidden="true" name="accion" value="search" />
                        <input type="submit" class="btn btn-primary" value="Buscar" />
                        <input type="reset" class="btn btn-primary" value="Limpiar" />
                        <a href="historico.jsp" target="principal" class="btn btn-info">Historial de Busqueda</a>
                    </td>
                </tr>
            </table>     
</html>
