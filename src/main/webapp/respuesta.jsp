
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrap.min.css" rel="stylesheet" type="text/css" />
        <title>Evaluacion Final</title>
    </head>
    <body>
        <% 
            String palabra = (String) request.getAttribute("palabra");
            String mensaje = (String) request.getAttribute("mensajeSalida");
        %>
        <DIV class="container">
            <table class="table">
                <tr>
                    <td class="text-center"><h3>Palabra buscada: <ol><%=palabra%></ol></h3></td>
                </tr>
                <tr>
                    <td class="text-left"><h3><ol><%=mensaje%></ol></h3></td>
                </tr>
                <tr>
                    <td class="text-center"><a class="btn btn-primary" align="center" href="index.jsp" target="_top">Volver</a></td>

                </tr>
            </table>
        </DIV>
        
        
    </body>
</html>