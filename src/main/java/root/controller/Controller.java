
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.util.logging.Level;
import java.util.logging.Logger;
import root.model.dao.PalabrasJpaController;
import root.persistence.entities.Palabras;
import root.services.PalabraRest;


@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           
            String palabra = request.getParameter("palabra");
            String origPalabra = palabra; 
            palabra = PalabraRest.eliminarAcentos(palabra);
            palabra = palabra.toLowerCase();
            

            String baseURL = "https://sjimenezef.herokuapp.com/api/palabra/";            
            OkHttpClient clienteWEB = new OkHttpClient();
            Request req = new Request.Builder().url(baseURL + palabra).get().addHeader("Content-Type","text/json;Charset=UTF-8").build();
            com.squareup.okhttp.Response respuesta = clienteWEB.newCall(req).execute();
            String strResp = respuesta.body().string(); 
            

            if (strResp.charAt(0) != '*') {
                
                PalabrasJpaController dao = new PalabrasJpaController(); 
                
                Palabras ingPalabra = new Palabras();
                ingPalabra.setPalabra(origPalabra);
                ingPalabra.setSignificado(strResp);
                
                

                Palabras checkExiste = dao.findPalabras(origPalabra); 
                if (checkExiste != null) {
                    dao.edit(ingPalabra);
                } else {
                    dao.create(ingPalabra);
                }
            }
            
            request.setAttribute("palabra", origPalabra);
            request.setAttribute("mensajeSalida", strResp);
            request.getRequestDispatcher("respuesta.jsp").forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}