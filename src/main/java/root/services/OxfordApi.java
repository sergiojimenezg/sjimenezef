
package root.services;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;


@Path("/ox")
public class OxfordApi {
    
    @GET 
    @Path("/{pBuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject buscarOX(@PathParam("pBuscar") String pBuscar) throws IOException{
        
        String baseURL = "https://od-api.oxforddictionaries.com/api/v2/entries/es/"; 
        String parametrosURL = "?fields=definitions&strictMatch=false";
        OkHttpClient clienteWEB = new OkHttpClient();
        Request request = new Request.Builder()
                .url(baseURL + pBuscar + parametrosURL)
                .get()
                .addHeader("Content-Type","text/json;Charset=UTF-8")
                .addHeader("app_id", "71424ce6")
                .addHeader("app_key", "71602fe536186df4a8d0d11291cd53bb")
                .build();
        com.squareup.okhttp.Response respuesta = clienteWEB.newCall(request).execute();
        
        try {
            JSONObject jsonParseado = new JSONObject(respuesta.body().string());
            JSONArray arrDefiniciones = jsonParseado.getJSONArray("results");

            JSONObject auxJson = arrDefiniciones.getJSONObject(0);
            JSONArray auxArrA = auxJson.getJSONArray("lexicalEntries");

            auxJson = auxArrA.getJSONObject(0);
            JSONArray auxArrB = auxJson.getJSONArray("entries");

            auxJson = auxArrB.getJSONObject(0);
            JSONArray auxArrC = auxJson.getJSONArray("senses");

            JSONObject jsonSalida = new JSONObject();
        
        String auxMsg = "";

            for (int i = 0; i < auxArrC.length(); i++) {
                auxMsg = auxArrC.getJSONObject(i).get("definitions").toString().substring(2);
                auxMsg = auxMsg.substring(0, auxMsg.length() - 2);
                jsonSalida.put("" + i, auxMsg);
            }
            System.out.println("*** JSON VALIDO ***");
            
            System.out.println(jsonSalida.toString());
            return jsonSalida; 
        } catch(Exception e) {
            JSONObject jsonSalida = new JSONObject();
            jsonSalida.put("0","*** ERROR ***: Palabra No Existe, intente con una nueva.");
            /** Solo LOG DE RESPUESTA INVALIDO **/
            System.out.println("*** JSON INVALIDO ***");
            
            System.out.println(jsonSalida.toString());
            return jsonSalida;
        }
        
    }
}
