
package root.services;

import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import net.minidev.json.parser.ParseException;
import org.json.JSONObject;

@Path("/palabra")
public class PalabraRest {
    
    @GET 
    @Path("/{pBuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarPalabra(@PathParam("pBuscar") String pBuscar) throws IOException, ParseException{

        pBuscar = eliminarAcentos(pBuscar);

        OxfordApi buscar = new OxfordApi();
        JSONObject jsonExt = buscar.buscarOX(pBuscar.toLowerCase());

        
        String descripcion = "";

        
        for (int i = 0; i < jsonExt.length(); i++){
            descripcion = descripcion + "<li>" + jsonExt.get(""+i).toString() + "</li>"; 
        }

        char cZero = descripcion.charAt(4); 
        if (cZero == '*') {
            descripcion = "*** Palabra: '" + pBuscar + "' no Existe, Intente con otra. ***";
        }         

        return Response.ok(200).entity(descripcion).build(); 
    }
    
    public static String eliminarAcentos(String str) {

		final String ORIGINAL = "ÁáÉéÍíÓóÚúÑñÜü";
		final String REEMPLAZO = "AaEeIiOoUuNnUu";

		if (str == null) {
			return null;
		}
		char[] array = str.toCharArray();
		for (int indice = 0; indice < array.length; indice++) {
			int pos = ORIGINAL.indexOf(array[indice]);
			if (pos > -1) {
				array[indice] = REEMPLAZO.charAt(pos);
			}
		}
		return new String(array);
	}
    
    
}
